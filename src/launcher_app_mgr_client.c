/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include "launcher.h"

extern ClutterActor   *stage;
static gboolean g_popup_status_clb( BarkwayService *pObject, const gchar *pArg_app_name, const gchar *pArg_title,
                const gchar *pArg_popup_status, gpointer pUserData )
{
        g_print("\n g_popup_status_clb \n");
        return TRUE;
}
/* *********************************************************************
 * Function Name :  g_confirmation_result_clb
 * Parameters   : *Object,AppName,title,confirmation result,confirmation value,userdata.
 * Description  : Confirmation result callback function for media info.
 * Return Value : boolean
 *********************************************************************** */
gboolean g_confirmation_result_clb(
                BarkwayService *object,
                const gchar *pArgAppName,
                const gchar *arg_title,
                const gchar *arg_confirmation_result,
                gint confirmation_value,gpointer userdata)
{
	//MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (userdata)->priv;

	if(g_strcmp0(pArgAppName , MILDENHALL_LAUNCHER_APP_NAME) == FALSE)
	{
	}
	return TRUE;
}
/* *********************************************************************
 * Function Name : g_error_msg_clb
 * Parameters   : *Object,AppName,Error Message.
 * Description  : Error Message callback function for app manager.
 * Return Value : boolean
 *********************************************************************** */
static gboolean g_error_msg_clb (
                BarkwayService *object,
                const gchar *pArgAppName,
                const gchar *arg_error_msg)
{
        g_print("\n g_error_msg_clb error is = %s \n",arg_error_msg);
        return TRUE;
}
/* *********************************************************************
 * Function Name :  media_info_popup_proxy_clb
 * Parameters   : *Object,*res,MoneyConvert Object.
 * Description  : Proxy callback function for Popup.
 * Return Value : boolean
 *********************************************************************** */
static void launcher_popup_proxy_clb( GObject *source_object,
                GAsyncResult *res,
                gpointer pUserData)
{
        g_print("\n launcher_popup_proxy_clb \n");
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (pUserData)->priv;
        if(!pUserData)
                return;
        GError *error=NULL;
        priv->popup_proxy=barkway_service_proxy_new_finish(res , &error);
        g_signal_connect(priv->popup_proxy,"popup-status",(GCallback)g_popup_status_clb,pUserData);
        g_signal_connect(priv->popup_proxy,"confirmation-result",(GCallback)g_confirmation_result_clb,pUserData);
        g_signal_connect(priv->popup_proxy,"error-msg",(GCallback)g_error_msg_clb,pUserData);
}



/* *********************************************************************
 * Function Name : on_popup_name_appeared
 * Parameters   : Connection Object,name,MoneyConvert Object.
 * Description  : Name appeared callback function for popup
 * Return Value : void
 *********************************************************************** */
static void
on_popup_name_appeared (GDBusConnection *connection,
                const gchar     *name,
                const gchar     *name_owner,
                gpointer         pUserData)
{
        g_print("\n on_popup_name_appeared \n");
        barkway_service_proxy_new(connection,
                        G_DBUS_PROXY_FLAGS_NONE,
                        "org.apertis.Barkway",
                        "/org/apertis/Barkway/Service",
                        NULL,
                        launcher_popup_proxy_clb,
                        pUserData);
}
/* *********************************************************************
 * Function Name : on_popup_name_vanished
 * Parameters   : Connection Object,name,MoneyConvert Object.
 * Description  : Name vanished callback function for popup
 * Return Value : void
 *********************************************************************** */
static void
on_popup_name_vanished(GDBusConnection *connection,
                const gchar     *name,
                gpointer         pUserData)
{
 MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (pUserData)->priv;
        if(!pUserData)
                return;
        g_print("\n name_vanished \n");
        if(NULL != priv->popup_proxy)
                g_object_unref(priv->popup_proxy);
}

void app_mgr_proxy_clb( GObject *source_object,
                        GAsyncResult *res,
                        gpointer user_data)
{
 GError *error;

 if(! MILDENHALL_LAUNCHER(user_data))
  return;

 MildenhallLauncher *launcher = MILDENHALL_LAUNCHER(user_data);
 MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;

 launcher_debug("app mgr service proxy new \n");
  /* finishes the proxy creation and gets the proxy ptr */
 priv->app_mgr_proxy = canterbury_app_manager_proxy_new_finish(res , &error);

 if(priv->app_mgr_proxy == NULL)
 {
   g_printerr("error %s \n",g_dbus_error_get_remote_error(error));
   return;
 }


	 g_bus_watch_name (G_BUS_TYPE_SESSION,
                        "org.apertis.Barkway",
                        G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                        on_popup_name_appeared,
                        on_popup_name_vanished,
                        user_data,
                        NULL);
  /* register for app manager signals */
  g_signal_connect (priv->app_mgr_proxy,
                    "new-app-state",
                    G_CALLBACK (new_app_status),
                    user_data);

  canterbury_app_manager_call_register_my_app(priv->app_mgr_proxy ,
    MILDENHALL_LAUNCHER_APP_NAME , 0, NULL , launcher_register_clb , user_data);
}

void
app_mgr_name_appeared (GDBusConnection *connection,
                       const gchar     *name,
                       const gchar     *name_owner,
                       gpointer         user_data)
{
  launcher_debug("app mgr service name_appeared \n");

  /* Asynchronously creates a proxy for the App manager D-Bus interface */
  canterbury_app_manager_proxy_new (
                                          connection,
                                          G_DBUS_PROXY_FLAGS_NONE,
                                          "org.apertis.Canterbury",
                                          "/org/apertis/Canterbury/AppManager",
                                          NULL,
                                          app_mgr_proxy_clb,
                                          user_data);
	/* Asynchronously creates a proxy for the D-Bus interface */
	canterbury_launcher_proxy_new (
			connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Canterbury",
			"/org/apertis/Canterbury/Launcher",
			NULL,
			launcher_proxy_clb,
			user_data );

                         /* Asynchronously creates a proxy for the App manager D-Bus interface */
  canterbury_hard_keys_proxy_new (
                                          connection,
                                          G_DBUS_PROXY_FLAGS_NONE,
                                          "org.apertis.Canterbury",
                                          "/org/apertis/Canterbury/HardKeys",
                                          NULL,
                                          app_mgr_hk_proxy_clb,
                                          user_data);

}


void
app_mgr_name_vanished( GDBusConnection *connection,
                       const gchar     *name,
                       gpointer         user_data)
{

 if(! MILDENHALL_LAUNCHER(user_data))
  return;

  MildenhallLauncher *launcher = MILDENHALL_LAUNCHER(user_data);
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;

 launcher_debug("app_mgr_name_vanished \n");

 g_clear_object (&priv->app_mgr_proxy);
}

void new_app_status( CanterburyAppManager  *object,
                     const gchar             *app_name,
                     gint                     new_app_state,
                     GVariant                *arguments,
                     gpointer                 user_data)
{
  if( g_strcmp0(MILDENHALL_LAUNCHER_APP_NAME ,app_name ) == FALSE )
  {
    switch (new_app_state)
    {
      case CANTERBURY_APP_STATE_START:
       launcher_debug("********Launcher is in normal state ******** \n" );
       break;

     case CANTERBURY_APP_STATE_BACKGROUND:
      /* see if it can do a clutter actor hide */
      launcher_debug("********Launcher is in background state ******** \n" );
      break;

     case CANTERBURY_APP_STATE_SHOW:
      /* check if the application needs to resynchronize with its server */
      launcher_debug("********Launcher is in show state ******** \n" );
      break;

     case CANTERBURY_APP_STATE_RESTART:
      /* check if the application needs to resynchronize with its server */
      launcher_debug("********Launcher is in restart state ******** \n" );
      break;

     case CANTERBURY_APP_STATE_OFF:
      /* store all persistence infornmation immediately.. */
      launcher_debug("********Launcher is in off state ******** \n" );
      break;

     case CANTERBURY_APP_STATE_PAUSE:
      /* store all persistence infornmation immediately..*/
      launcher_debug("********Launcher is in pause state ******** \n" );
      break;

     default:
      launcher_debug("Launcher app in unknown state ???  \n" );
      break;
    }
  }
}

void launcher_register_clb( GObject *source_object,
                            GAsyncResult *res,
                            gpointer user_data)
{
  gboolean return_val;
  GError *error = NULL;

 if(! MILDENHALL_LAUNCHER(user_data))
  return;

 MildenhallLauncher *launcher = MILDENHALL_LAUNCHER(user_data);
 MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
 return_val = canterbury_app_manager_call_register_my_app_finish(priv->app_mgr_proxy , res , &error);

  if(return_val == FALSE)
  {
     g_printerr ("error %s", error->message);
     g_clear_error (&error);
  }
}
