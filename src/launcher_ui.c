/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* *********************************************************************
                Standard Header Files
 *********************************************************************** */
#include <liblightwood-roller.h>
#include <liblightwood-fixedroller.h>
#include <liblightwood-varroller.h>

#include <clutter/gdk/clutter-gdk.h>
/* *********************************************************************
                User Header Files
 *********************************************************************** */
#include "launcher.h"
#include "sample-item.h"
#include "icon-label-icon-item.h"
#include <mildenhall/mildenhall.h>

extern ClutterActor   *stage;
const ClutterColor static NormalColor = {0x00, 0x00, 0x00, 0x00};
gchar *default_quick_menu_icon = "icon_music_cd-player_AC.png";

gchar *default_icons[12] = 
{
	"icon_music_AC.png",
	"icon_radio_AC.png",
	"icon_video&TV_AC.png",
	"icon_pictures_AC.png",
	"icon_internet_AC.png",
	"icon_travel_AC.png",
	"icon_communication_AC.png",
	"icon_games_AC.png",
	"icon_peripherials_AC.png",
	"icon_settings_AC.png",
        NULL
};

gchar *default_categories[12] =
{
        "M U S I C",
        "R A D I O",
        "V I D E O  &  T V",
        "P I C T U R E S",
        "I N T E R N E T",
        "T R A V E L",
        "C O M M U N I C A T I O N",
        "G A M E S",
        "P R O D U C T I V I T Y",
        "S E T T I N G S",
        NULL
};

gchar *first_quick_menu_categories[12] =
{
        "A R T I S T S",
        "T E R R E S T.  R A D I O",
        "V I D E O S / T I T L E",
        "P I C T U R E S",
        "B R O W S E R",
        "N A V I G A T I O N",
        "A D D R E S S B O O K",
        "S U D O K U",
        "D O C  V I E W E R",
        "S E T T I N G S",
        NULL
};
gchar *default_quick_menu_items[30] =
{
        "A R T I S T S",
        "A L B U M S",
        "S O N G S",
        "M E D I A  I N F O",
        "H T M L 5  A U D I O",
        "T E R R E S T.  R A D I O",
        "L A S T  F M",
        "V I D E O S / T I T L E",
        "I N T E R N E T  V I D E O",
        "P I C T U R E S / T I T L E",
        "P I C T U R E S / T A G",
        "P I C T U R E S / R E S",
        "P I C T U R E S / D A T E",
        "P A N O R A M I O",
        "B R O W S E R",
        "B O O K M A R K S",
        "N E W S",
        "N A V I G A T I O N",
        "G O O G L E  E A R T H",
        "W E A T H E R",
        "E M A I L",
        "I M",
        "S U D O K U",
        "C H E S S",
        "D O C  V I E W E R",
        "V O I C E  N O T E S",
        "U T I L S",
        "A P P  S E T T I N G S",
        "A P P S T O R E",
        NULL
};

enum
{
        COLUMN_LEFT_ICON_IS_TEXT,
        COLUMN_LEFT_ICON_WIDTH,
        COLUMN_LEFT_ICON_ID,
        COLUMN_LEFT_ICON_INFO,
        COLUMN_RIGHT_ICON_IS_TEXT,
        COLUMN_RIGHT_ICON_WIDTH,
        COLUMN_RIGHT_ICON_ID,
        COLUMN_RIGHT_ICON_INFO,
        COLUMN_TEXT_BOX_DEFAULT_TEXT,
        COLUMN_ENTRY_ID,
        SPELLER_COLUMN_LAST
};

static void
roller_scroll_started_cb (MildenhallRollerContainer *roller,
                          gpointer user_data)
{
  MildenhallLauncher *launcher = MILDENHALL_LAUNCHER (user_data);
  MildenhallLauncherPrivate *priv = launcher->priv;

  if (roller == (MildenhallRollerContainer *)priv->master_roller)
    priv->is_master_roller_scrolling = TRUE;

  if (roller == (MildenhallRollerContainer *)priv->slave_roller)
    priv->is_slave_roller_scrolling = TRUE;
}

static void
roller_scroll_finished_cb (MildenhallRollerContainer *roller,
                           guint first_visible_item,
                           guint total_visible_items,
                           gpointer user_data)
{
  MildenhallLauncher *launcher = MILDENHALL_LAUNCHER (user_data);
  MildenhallLauncherPrivate *priv = launcher->priv;

  if (roller == (MildenhallRollerContainer *)priv->master_roller)
    priv->is_master_roller_scrolling = FALSE;

  if (roller == (MildenhallRollerContainer *)priv->slave_roller)
    priv->is_slave_roller_scrolling = FALSE;
}

/* *********************************************************************
 * Function Name : master_locking_finished_cb
 * Parameters   :Master roller and quick menu roller.
 * Description  :callback function connected to the signal::locking finished
			from Main menu roller.
		For the focused child in the  Main menu corresponding entry
		in the quick menu need to be focused.
*********************************************************************** */
static void
master_locking_finished_cb (MildenhallRollerContainer *master_roller,
                            MildenhallLauncher *launcher)
{
	gint focused_row;
	gint focused_row_slave;
	gchar *value;
	gchar *value_slave;
	guint slaveRow;
	ThornburyModel *model;
	ThornburyModel *slave_model;
	MildenhallRollerContainer *slave_roller;
	MildenhallLauncherPrivate *priv;

	g_return_if_fail (launcher != NULL);

	priv = MILDENHALL_LAUNCHER (launcher)->priv;
        slave_roller = (MildenhallRollerContainer *)priv->slave_roller;

	/* For the same child,if this call back is called then do nothing and return */
	if (priv->MainMenufocus && !priv->is_master_roller_scrolling)
	{
		priv->MainMenufocus = FALSE;
		return;
	}
	/* Get the focused row  and model of the Master roller for further implementation */
	g_object_get (G_OBJECT (master_roller), "focused-row", &focused_row, NULL);
	g_object_get (G_OBJECT (slave_roller), "focused-row", &focused_row_slave, NULL);
	g_object_get (G_OBJECT (master_roller), "model", &model, NULL);
	/* Get the model of the quick menu roller */
	launcher_debug("%s focused-row = %d\n",__FUNCTION__,focused_row);
	g_object_get (G_OBJECT (slave_roller), "model", &slave_model, NULL);
		ThornburyModelIter *iter_slave = thornbury_model_get_iter_at_row (slave_model,focused_row_slave);
			thornbury_model_iter_get(iter_slave,0,&value_slave,-1);
	if(model)
	{
		/* Get the corresponding iterative for the Master roller model */
		ThornburyModelIter *iter = thornbury_model_get_iter_at_row (model,focused_row);
		if(iter)
		{
			/*Get the name of the child which is key value in hashtable*/
			thornbury_model_iter_get(iter,0,&value,-1);
			if(value)
			{
				//printf(" Name of the string in Master-key%s\n", value);
				/* For Key get the corresponding Pair in the hashtable */
				gchar *quick_menu = g_strdup(g_hash_table_lookup(priv->MainToQuickmap,value));
	launcher_debug("%s quick_menu = %s\n",__FUNCTION__,quick_menu);
				if(quick_menu)
				{
					//printf(" Name of the string in Master-pair%s\n", quick_menu);
					gchar *quickstr = NULL;
					/* Get the exact row from the Quick menu Model for the pair Matched and
					   set it to focus in quick menu roller*/
					ThornburyModelIter *iter = thornbury_model_get_first_iter(slave_model);
					while (!thornbury_model_iter_is_last (iter))
					{

						thornbury_model_iter_get(iter,0,&quickstr,-1);
						if(g_strcmp0(quickstr,quick_menu) == 0)
						{
							slaveRow = thornbury_model_iter_get_row(iter);
							g_object_set (G_OBJECT (slave_roller), "focused-row", slaveRow, NULL);
							/* set the flag when QuickMenu is on focus */
							priv->QuickMenufocus = TRUE;
							g_free(quickstr);
							break;
						}
						g_free(quickstr);
						iter = thornbury_model_iter_next (iter);
					}
					g_object_unref (iter);
					g_free(quick_menu);

				}
				g_free(value);
			}
		}
		g_object_unref (iter);

	}

}
/* *********************************************************************
 * Function Name : quick_menu_locking_finished_cb
 * Parameters   :quick menu roller and Master roller .
 * Description  :callback function connected to the signal::locking finished
                        from quick menu roller.
*********************************************************************** */

static void
quick_menu_locking_finished_cb (MildenhallRollerContainer *slave_roller,
                                MildenhallLauncher *launcher)
{
	gint focused_row;
	gint focused_row_master;
	gchar *value;
	gchar *value_focus;
	gchar *value_master;
	guint slaveRow;
	ThornburyModel *model;
	ThornburyModel *slave_model;
	MildenhallRollerContainer *master_roller;
	MildenhallLauncherPrivate *priv;

	g_return_if_fail (launcher != NULL);

	priv = MILDENHALL_LAUNCHER (launcher)->priv;
	master_roller = (MildenhallRollerContainer *)priv->master_roller;

	g_object_get (G_OBJECT (slave_roller), "focused-row", &focused_row, NULL);
	g_object_get (G_OBJECT (master_roller), "focused-row", &focused_row_master, NULL);
	g_object_get (G_OBJECT (master_roller), "model", &model, NULL);
	g_object_get (G_OBJECT (slave_roller), "model", &slave_model, NULL);
	launcher_debug("%s focused-row = %d\n",__FUNCTION__,focused_row);
		ThornburyModelIter *iter_master = thornbury_model_get_iter_at_row (model,focused_row_master);
			thornbury_model_iter_get(iter_master,0,&value_master,-1);
	if (priv->QuickMenufocus && !priv->is_slave_roller_scrolling)
	{
		priv->QuickMenufocus = FALSE;
		return;
	}
	if(model)
	{
		ThornburyModelIter *iter = thornbury_model_get_iter_at_row (slave_model,focused_row);
		if(iter)
		{
			thornbury_model_iter_get(iter,0,&value,-1);
			if(value)
			{
				//printf(" Name of the string in Slave-key%s\n", value);
				gchar *quick_menu = g_strdup(g_hash_table_lookup(priv->QuickToMain,value));
	launcher_debug("%s quick_menu = %s\n",__FUNCTION__,quick_menu);
				if(quick_menu)
				{
					//printf(" Name of the string in slave-pair%s\n", quick_menu);
					gchar *quickstr = NULL;
					ThornburyModelIter *iter = thornbury_model_get_first_iter(model);
					while (!thornbury_model_iter_is_last (iter))
					{

						thornbury_model_iter_get(iter,0,&quickstr,-1);
						if(g_strcmp0(quickstr,quick_menu) == 0)
						{
							slaveRow = thornbury_model_iter_get_row(iter);
							thornbury_model_iter_get(iter,0,&value_focus,-1);

								if(g_strcmp0(value_master,quick_menu) == 0)
								{
									//priv->QuickMenufocus = FALSE;
									return;
								}
							g_object_set (G_OBJECT (master_roller), "focused-row", slaveRow, NULL);
							priv->MainMenufocus = TRUE;
							g_free(quickstr);
							break;
						}
						g_free(quickstr);
						iter = thornbury_model_iter_next (iter);
					}
					g_object_unref (iter);
					g_free(quick_menu);

				}
				g_free(value);
			}
		}
		g_object_unref (iter);
	}
}
/* *********************************************************************
 * Function Name 	:create_main_menu_model
 * Parameters   	:void
 * Description  	:create the model in which data for the
				rollers items are available.
 * Return 		:ThornburyModel *
*********************************************************************** */

ThornburyModel *create_main_menu_model (MildenhallLauncher *launcher , gchar **main_menu_label, gchar **main_menu_icon, gchar **quick_menu_label, gchar **quick_menu_icon)
{
	ThornburyModel *model;
	gchar **p_main_menu ,  **p_main_menu_icon , **p_quick_menu ;
	gchar *pIconPath = NULL ;
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;

	priv->MainToQuickmap = g_hash_table_new_full(g_str_hash,g_str_equal,g_free, g_free);

  model = (ThornburyModel *)thornbury_list_model_new (3, G_TYPE_STRING, NULL,G_TYPE_STRING, NULL, G_TYPE_STRING, NULL,-1);
  gchar **p_default_categories = NULL;
  gchar **p_default_icons = NULL;
  guint count = 0;
	guint first_quick_menu_count = 0;
  for( p_default_categories = default_categories,p_default_icons = default_icons; *p_default_categories && *p_default_icons; p_default_categories++,p_default_icons ++,first_quick_menu_count ++ )
  {
      for( p_main_menu = main_menu_label , p_quick_menu = quick_menu_label , p_main_menu_icon = main_menu_icon ;
       *p_main_menu && *p_quick_menu && *p_main_menu_icon ;
       p_main_menu++ , p_quick_menu++ , p_main_menu_icon++ )
      {

        if( ( is_item_in_model( model , *p_main_menu ) == FALSE) && ( g_strcmp0( *p_default_categories , *p_main_menu ) == FALSE ) )
        {
          launcher_debug( "add new main item %s \n" , *p_main_menu  );

          LAUNCHER_CHECK_IMAGE_PATH(*p_main_menu_icon , pIconPath);
          thornbury_model_append (model, ILI_COLUMN_NAME, *p_main_menu, ILI_COLUMN_ICON,pIconPath,ILI_COLUMN_LABEL, *p_main_menu,-1);
          LAUNCHER_FREE_MEM_IF_NOT_NULL(pIconPath);
          gchar **p_first_quick_menu = NULL;
          for( p_first_quick_menu = quick_menu_label;*p_first_quick_menu ;p_first_quick_menu++ )
          {
             if( ( g_strcmp0( first_quick_menu_categories[count] , *p_first_quick_menu ) == FALSE ) )
             {
                break;
             }
          }
          if (*p_first_quick_menu == NULL)
	  {
	    if (!g_hash_table_contains (priv->MainToQuickmap, *p_main_menu))
	      g_hash_table_insert (priv->MainToQuickmap, g_strdup (*p_main_menu), g_strdup (*p_quick_menu));
	  }
	  else
	  {
	    if (!g_hash_table_contains(priv->MainToQuickmap, *p_main_menu))
	      g_hash_table_insert (priv->MainToQuickmap, g_strdup (*p_main_menu), g_strdup (*p_first_quick_menu));
	  }
	}
      }
      count++;
  }

  for( p_main_menu = main_menu_label , p_quick_menu = quick_menu_label , p_main_menu_icon = main_menu_icon ;
       *p_main_menu && *p_quick_menu && *p_main_menu_icon ;
       p_main_menu++ , p_quick_menu++ , p_main_menu_icon++ )
  {

    if(is_item_in_model(model ,*p_main_menu ))
    continue;

    launcher_debug( "create_main_menu_model %s  %s\n" , *p_main_menu , *p_main_menu_icon);

    LAUNCHER_CHECK_IMAGE_PATH(*p_main_menu_icon , pIconPath);
    thornbury_model_append (model, ILI_COLUMN_NAME, *p_main_menu, ILI_COLUMN_ICON, pIconPath,ILI_COLUMN_LABEL, *p_main_menu,-1);

    if (!g_hash_table_contains (priv->MainToQuickmap, *p_main_menu))
      g_hash_table_insert (priv->MainToQuickmap, g_strdup (*p_main_menu), g_strdup (*p_quick_menu));
  }

	//launcher_debug("main model=%d",thornbury_model_get_n_rows(model));
	return model;
}

/* *********************************************************************
 * Function Name 	:create_quick_menu_model
 * Parameters   	:void
 * Description  	:create the model in which data for the
				rollers items are available.
 * Return 		:ThornburyModel *
*********************************************************************** */

ThornburyModel *create_quick_menu_model(MildenhallLauncher *launcher , gchar **main_menu_label, gchar **main_menu_icon, gchar **quick_menu_label, gchar **quick_menu_icon)
{
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
	ThornburyModel *model;
	gchar *pIconPath = NULL ;
	gchar **p_main_menu ,  **p_quick_menu_icon , **p_quick_menu ;

	model = (ThornburyModel *)thornbury_list_model_new (7,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_FLOAT , NULL ,
			G_TYPE_UINT,0,
			-1);

	priv->QuickToMain = g_hash_table_new_full(g_str_hash,g_str_equal,g_free, g_free);

	gchar **p_default_quick_menu_items = NULL;
#if 0
	for( p_default_categories = default_categories,p_default_icons = default_icons; *p_default_categories,*p_default_icons; p_default_categories++,p_default_icons ++ )
	{
		thornbury_model_append (model, ILI_COLUMN_NAME, "--", ILI_COLUMN_ICON,default_quick_menu_icon , ILI_COLUMN_LABEL, "--",
				ILI_COLUMN_ICON2, " ",ILI_COLUMN_STATUS_TEXT , " " , ILI_COLUMN_PROGRESS , 0.0 ,ILI_COLUMN_LAUNCH_ACTION ,APP_LAUNCH_DISABLED ,-1);
		g_hash_table_insert(priv->QuickToMain,g_strdup(g_str_concate(*p_default_categories,"__",NULL),g_strdup(*p_default_categories)));
	}
#endif
	for( p_default_quick_menu_items = default_quick_menu_items; *p_default_quick_menu_items ; p_default_quick_menu_items++ )
	{

		for( p_main_menu = main_menu_label , p_quick_menu = quick_menu_label , p_quick_menu_icon = quick_menu_icon ;
				*p_main_menu && *p_quick_menu && *p_quick_menu_icon ;
				p_main_menu++ , p_quick_menu++ , p_quick_menu_icon++ )
		{
			//launcher_debug( "quick item ..%s.. def ..%s.. \n" , *p_quick_menu  , *p_default_quick_menu_items );

			if( ( is_item_in_model( model , *p_quick_menu ) == FALSE) && ( g_strcmp0( *p_default_quick_menu_items , *p_quick_menu ) == FALSE ) )
			{
				launcher_debug( "add new quick item %s \n" , *p_quick_menu );
				LAUNCHER_CHECK_IMAGE_PATH(*p_quick_menu_icon , pIconPath);
				thornbury_model_append (model, ILI_COLUMN_NAME, *p_quick_menu, ILI_COLUMN_ICON, pIconPath, ILI_COLUMN_LABEL, *p_quick_menu,
						ILI_COLUMN_ICON2, pIconPath,ILI_COLUMN_STATUS_TEXT , " " , ILI_COLUMN_PROGRESS , 0.0 ,ILI_COLUMN_LAUNCH_ACTION ,APP_LAUNCH_ENABLED ,-1);
				LAUNCHER_FREE_MEM_IF_NOT_NULL(pIconPath);
#if 0
				gchar *key = g_strconcat(main_menu,"--",NULL);
				gchar *result_key;
				g_hash_table_lookup_extended(priv->QuickToMain,key,&result_key,NULL);		
				if(result_key != NULL)	
				{
					g_hash_table_remove(priv->QuickToMain,result_key);
					thornbury_model_remove()

					if(result_key != NULL)
					{
						g_free(result_key);
						result_key = NULL;
					}

				}
				if(key != NULL)
				{
					g_free(key);
					key = NULL;
				}
#endif
				g_hash_table_insert(priv->QuickToMain,g_strdup(*p_quick_menu),g_strdup(*p_main_menu));
			}
		}
	}

	for( p_main_menu = main_menu_label , p_quick_menu = quick_menu_label , p_quick_menu_icon = quick_menu_icon ;
			*p_main_menu && *p_quick_menu && *p_quick_menu_icon ;
			p_main_menu++ , p_quick_menu++ , p_quick_menu_icon++ )
	{

		if(is_item_in_model(model ,*p_quick_menu ))
			continue;
		LAUNCHER_CHECK_IMAGE_PATH(*p_quick_menu_icon , pIconPath);
		launcher_debug( "create_quick_menu_model %s  %s\n" , *p_quick_menu , pIconPath);
		insert_quick_menu_entry_into_model (launcher, 
						    model,
						    *p_main_menu,
						    *p_quick_menu, 
					            *p_quick_menu_icon, 
						    NULL, 
						    0.0);
	}
	
	return model;
}

void on_notify_current_active_menu_entry ( GObject    *object,
                                           GParamSpec *pspec,
                                           gpointer    user_data)
{
  gint focussed_row;

 if(! MILDENHALL_LAUNCHER(user_data))
  return;

 MildenhallLauncher *launcher = MILDENHALL_LAUNCHER(user_data);
 MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
 guint no_of_rows = thornbury_model_get_n_rows(priv->quick_menu_model);
 const gchar* menu_entry =  canterbury_launcher_get_current_active_menu_entry(priv->launcher_proxy);
  if( ( 0 == no_of_rows ) || ( menu_entry == NULL ) )
   return;

  focussed_row = mildenhall_roller_container_get_focused_row (MILDENHALL_ROLLER_CONTAINER (priv->slave_roller));
  ThornburyModelIter *iter = thornbury_model_get_iter_at_row (priv->quick_menu_model, focussed_row);
  gchar* value = NULL;
  thornbury_model_iter_get (iter, 0, &value , -1);
  if( ( value == NULL ) || ( g_strcmp0( value , menu_entry ) == FALSE ) )
  return;

  guint cnt = 0;

  for( cnt = 0 ; cnt < no_of_rows ; cnt++ )
  {
    ThornburyModelIter *iter = thornbury_model_get_iter_at_row (priv->quick_menu_model, cnt);
    gchar* value = NULL;
    thornbury_model_iter_get (iter, 0, &value , -1);

    if( g_strcmp0( value , menu_entry ) == FALSE )
    {
      launcher_debug( " focus item val name %s \n" , (gchar*)value);
      mildenhall_roller_container_set_focused_row (MILDENHALL_ROLLER_CONTAINER (priv->slave_roller), cnt, TRUE);
      g_free(value);
      break;
    }
    g_free(value);
  }
}

/* *********************************************************************
 * Function Name 	:create_roller
 * Parameters   	:model
 * Description  	:creates the roller
 * Return 		:ClutterActor *
*********************************************************************** */

ClutterActor *
create_roller (ThornburyModel *model)
{
	ClutterActor *roller;
	ThornburyItemFactory *itemFactory;
        GObject *pObject = NULL;

	itemFactory = thornbury_item_factory_generate_widget_with_props (
	  MILDENHALL_TYPE_ROLLER_CONTAINER, PKGDATADIR "/launcher_main_roller_prop.json");

        g_object_get(itemFactory, "object", &pObject, NULL);
        roller = CLUTTER_ACTOR(pObject);

        g_object_set (pObject,
                      "item-type", MILDENHALL_TYPE_SAMPLE_ITEM,
                      "model", model,
                      NULL);

        mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "name", ILI_COLUMN_NAME);
        mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "label", ILI_COLUMN_LABEL);
        mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "icon", ILI_COLUMN_ICON);

	return roller;
}

/* *********************************************************************
 * Function Name 	:create_roller
 * Parameters   	:model
 * Description  	:creates the roller
 * Return 		:ClutterActor *
*********************************************************************** */

ClutterActor *
create_slave_roller (ThornburyModel *model)
{
  ClutterActor *roller;
  ThornburyItemFactory *itemFactory;
  GObject *pObject = NULL;

  itemFactory = thornbury_item_factory_generate_widget_with_props 
    (MILDENHALL_TYPE_ROLLER_CONTAINER, PKGDATADIR "/launcher_slave_roller_prop.json");

  g_object_get(itemFactory, "object", &pObject, NULL);
  roller = CLUTTER_ACTOR(pObject);

  g_object_set(pObject,  "item-type", TYPE_ICON_LABEL_ICON_ITEM, "model", model,  NULL);

  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "name", ILI_COLUMN_NAME);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "label", ILI_COLUMN_LABEL);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "icon", ILI_COLUMN_ICON);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "icon2", ILI_COLUMN_ICON2);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "status-label", ILI_COLUMN_STATUS_TEXT);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "progress", ILI_COLUMN_PROGRESS);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller), "extra-opt", ILI_COLUMN_LAUNCH_ACTION);

  return roller;
}

static void
roller_item_activated_cb (MildenhallRollerContainer *roller, guint row, gpointer data)
{
  ThornburyModel *model = mildenhall_roller_container_get_model (roller);

  ThornburyModelIter *iter = thornbury_model_get_iter_at_row (model, row);
  gchar* quick_app_name = NULL;
  guint uinLaunchStatus = APP_LAUNCH_ENABLED ;
  thornbury_model_iter_get (iter, 0, &quick_app_name ,ILI_COLUMN_LAUNCH_ACTION, &uinLaunchStatus, -1);
  launcher_debug( "row name %s %d  \n" , (gchar*)quick_app_name , row);

  if (uinLaunchStatus != APP_LAUNCH_DISABLED && quick_app_name != NULL)
    {
      mildenhall_launcher_activate_by_display_name (data, quick_app_name,
                                                    CANTERBURY_LAUNCHER_CLIENT);
    }
  else
    {
      launcher_debug ("Entry point “%s” disabled from launching", quick_app_name);
    }
}

gboolean set_rollers_to_focus(gpointer data)
{
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (data)->priv;

  mildenhall_roller_container_set_focused_row (MILDENHALL_ROLLER_CONTAINER (priv->master_roller), 0, TRUE);

  return FALSE;
}

ThornburyModel *create_navi_drawer_model()
{
        ThornburyModel *model = NULL;

        model = (ThornburyModel *)thornbury_list_model_new (DRAWER_COLUMN_LAST,
                                      G_TYPE_STRING, NULL,
                                      G_TYPE_STRING, NULL,
                                      G_TYPE_STRING, NULL,
                                      G_TYPE_BOOLEAN, NULL,
                                      -1);

        thornbury_model_append (model, DRAWER_COLUMN_NAME, "BACK",
                            DRAWER_COLUMN_ICON, PKGDATADIR"/icon_back.png",
                            DRAWER_COLUMN_TOOLTIP_TEXT, "BACK",
                            DRAWER_COLUMN_REACTIVE, TRUE,
                            -1);
        thornbury_model_append (model, DRAWER_COLUMN_NAME, "NOW PLAYING",
                            DRAWER_COLUMN_ICON, PKGDATADIR"/icon_nowplaying_AC.png",
                            DRAWER_COLUMN_TOOLTIP_TEXT, "NOW PLAYING",
                            DRAWER_COLUMN_REACTIVE, TRUE,
                            -1);
        thornbury_model_append (model,  DRAWER_COLUMN_NAME, "HOME",
                            DRAWER_COLUMN_ICON, PKGDATADIR"/icon_home.png",
                            DRAWER_COLUMN_TOOLTIP_TEXT, "HOME",
                            DRAWER_COLUMN_REACTIVE, TRUE,
                            -1);


        return model;

}


void handle_back_press( CanterburyHardKeys  *object,
                        gpointer                 user_data)
{

}

void handle_now_playing_response( CanterburyHardKeys  *object,
                                  gchar*                   pAppName,
                                  gchar*                   menu_entry,
                                  gpointer                 user_data)
{
  launcher_debug("handle_now_playing_response menu_entry %s \n", menu_entry);
  mildenhall_launcher_activate_by_display_name (user_data, menu_entry,
                                                CANTERBURY_NOW_PLAYING_KEY);
}

void
app_mgr_hk_proxy_clb( GObject *source_object,
                      GAsyncResult *res,
                      gpointer user_data)
{
  GError *error;

 MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (user_data)->priv;

  launcher_debug("app_mgr_hk_proxy_clb \n");
  /* finishes the proxy creation and gets the proxy ptr */
  priv->app_mgr_hk_proxy = canterbury_hard_keys_proxy_new_finish(res , &error);

  if(priv->app_mgr_hk_proxy == NULL)
  {
    g_printerr("error %s \n",g_dbus_error_get_remote_error(error));
    return;
  }

  /* register for app manager signals */
  g_signal_connect (priv->app_mgr_hk_proxy,
                    "handle-back-press",
                    G_CALLBACK (handle_back_press),
                    user_data);

  g_signal_connect (priv->app_mgr_hk_proxy,
                    "now-playing-press-response",
                    G_CALLBACK (handle_now_playing_response),
                    user_data);

}
#if 0
void
app_mgr_hk_name_appeared (GDBusConnection *connection,
                          const gchar     *name,
                          const gchar     *name_owner,
                          gpointer         user_data)
{
  launcher_debug("app_mgr_hk_name_appeared\n");

  /* Asynchronously creates a proxy for the App manager D-Bus interface */
  canterbury_hard_keys_proxy_new (
                                          connection,
                                          G_DBUS_PROXY_FLAGS_NONE,
                                          "canterbury.HardKeys",
                                          "/canterbury/HardKeys",
                                          NULL,
                                          app_mgr_hk_proxy_clb,
                                          user_data);
}


void
app_mgr_hk_name_vanished( GDBusConnection *connection,
                          const gchar     *name,
                          gpointer         user_data)
{

  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (user_data)->priv;

  launcher_debug("app_mgr_hk_name_vanished \n");
  if(NULL != priv->app_mgr_hk_proxy)
   g_object_unref(priv->app_mgr_hk_proxy);
}
#endif


#if 0
void init_hardkey_client_handler(MildenhallLauncher *launcher)
{
  g_bus_watch_name (G_BUS_TYPE_SESSION,
                    "org.apertis.Canterbury.HardKeys",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    app_mgr_hk_name_appeared,
                    app_mgr_hk_name_vanished,
                    launcher,
                    NULL);

}
#endif

static void v_mildenhall_launcher_expand_stage(gpointer pUserData)
{
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (pUserData)->priv;
	gfloat fltTotalWidth = 0.0 , fltButtonWidth = 0.0, fltHeight = 0.0  ;
	cairo_region_t *region;
	cairo_rectangle_int_t rectangle;
	GdkWindow *win;

	g_object_get(G_OBJECT(priv->drawer) , "tooltip-width", &fltTotalWidth ,"width", &fltButtonWidth , "height", &fltHeight ,   NULL );
	//	fltTotalWidth = fltButtonWidth - fltTotalWidth ;
	g_object_set(CLUTTER_ACTOR(priv->slave_roller),  "width", 456.0,   NULL);
	clutter_actor_set_background_color(CLUTTER_ACTOR( stage), &NormalColor);
	clutter_actor_set_size (stage,(WIDTH+fltTotalWidth),HEIGHT);
	clutter_actor_queue_redraw (CLUTTER_ACTOR( stage));
	clutter_actor_set_reactive(CLUTTER_ACTOR( stage) , FALSE );

	rectangle.x = 0;
	rectangle.y = 0;
	rectangle.width = WIDTH + fltTotalWidth;
	rectangle.height = HEIGHT;

	region = cairo_region_create_rectangle (&rectangle);

	rectangle.x = WIDTH;
	rectangle.y = 0;
	rectangle.width = fltTotalWidth;
	rectangle.height = HEIGHT - fltHeight;

	cairo_region_subtract_rectangle (region, &rectangle);

	win = clutter_gdk_get_stage_window (CLUTTER_STAGE(stage));
	gdk_window_shape_combine_region (win, region, 0, 0);

	g_object_unref (win);
	cairo_region_destroy (region);
}

#if 0
/********************************************************
 * Function : v_popup_show_selection_popup_clb
 * Description: The callback function for the selection popup
 * Parameters:  pSource_object,GAsyncResult,gpointer
 * Return value: void
 ********************************************************/
static void v_popup_show_selection_popup_clb( GObject *pSource_object, GAsyncResult *pRes, gpointer pUser_data)
{
        /* call back function for show dbus-method */
        GError *pError = NULL;
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER(pUser_data)->priv;
        //canterbury_mutter_plugin_call_stop_idle_animation(mediaInfo->priv->mutter_proxy,NULL,idle_animation_stop_cb,pUser_data);
        barkway_service_call_selection_popup_finish( priv->popup_proxy, pRes, &pError );

}
static void
_free_result_list(gpointer userData)
{
	SearchData *sData = (SearchData *)userData;
	if(sData)
	{
		if(sData->pSearchString)
		{
			g_free(sData->pSearchString);
			sData->pSearchString = NULL;
		}
		if(sData->pName)
		{
			g_free(sData->pName);
                        sData->pName = NULL;
		}
		if(sData->pIcon)
		{
			g_free(sData->pIcon);
                        sData->pIcon = NULL;
		}
		if(sData->uId)
		{
			g_free(sData->uId);
                        sData->uId = NULL;
		}
		if(sData->pCategoySearchId)
		{
			g_free(sData->pCategoySearchId);
                        sData->pCategoySearchId = NULL;
		}
		g_free(sData);
		sData = NULL;
	}
}
#endif
#if 0
void update_selection_popup(MildenhallLauncher *launcher,SearchData *sData, gboolean update )
{
	MildenhallLauncherPrivate *priv = launcher->priv;
	/* one line data on the header of selection popup */
	GVariantBuilder *pGvb = g_variant_builder_new ( G_VARIANT_TYPE ("a{ss}") );
	g_variant_builder_add ( pGvb, "{ss}","static",g_strdup(MILDENHALL_POPUP_GLOBAL_SEARCH_QUERY_STRING) );
	GVariant* pDisplay_Text = g_variant_builder_end (pGvb);

	/* Icons */
	GVariantBuilder *pGvb3 = g_variant_builder_new ( G_VARIANT_TYPE ("a{ss}") );
	gchar *pSearchIn = g_strconcat(PKGDATADIR,"/icon_search.png",NULL);
	gchar *pMusicIn = g_strconcat(PKGDATADIR,"/icon_search.png",NULL);
	g_variant_builder_add ( pGvb3, "{ss}","AppIcon",pSearchIn );
	g_variant_builder_add ( pGvb3, "{ss}","MsgIcon",pMusicIn );
	GVariant* image = g_variant_builder_end (pGvb3);

	/* */
	GVariantBuilder *prowInfo = g_variant_builder_new ( G_VARIANT_TYPE_ARRAY );
	GVariantBuilder *pGvb1 = g_variant_builder_new ( G_VARIANT_TYPE ("a{ss}") );
	g_variant_builder_add ( pGvb1, "{ss}","RowId",sData->pCategoySearchId);
	g_variant_builder_add ( pGvb1, "{ss}","Image",sData->pIcon);
	g_variant_builder_add ( pGvb1, "{ss}","Text",sData->pName);
	g_variant_builder_add ( pGvb1, "{ss}","Text",sData->pCategory);
	//g_variant_builder_add ( pGvb1, "{ss}","Text",NULL);
	GVariant* pOneItemInfo = g_variant_builder_end (pGvb1);
	g_variant_builder_add_value (prowInfo,pOneItemInfo);
	GVariant*  pRows_info = g_variant_builder_end (prowInfo);
	if(update == FALSE)
	{
		barkway_service_call_selection_popup(priv->popup_proxy,MILDENHALL_LAUNCHER_APP_NAME,"global-search-selection-popup",pDisplay_Text,pRows_info,
				image,NULL,v_popup_show_selection_popup_clb,launcher);
		if(priv->pSearchResultList)
		{
			g_list_free_full(priv->pSearchResultList,_free_result_list);
			priv->pSearchResultList = NULL;
		}
		priv->pSearchResultList = g_list_append(priv->pSearchResultList,sData);
	}
	else
	{
		barkway_service_call_update_selection_popup_rows(priv->popup_proxy,MILDENHALL_LAUNCHER_APP_NAME, "global-search-selection-popup", pRows_info, FALSE, NULL, v_popup_show_selection_popup_clb, launcher);
		priv->pSearchResultList = g_list_append(priv->pSearchResultList,sData);
	}

}

#endif


static void v_drawer_button_open_state_clb (GObject *pActor,  gpointer pUserData)
{
	launcher_debug("\n\n%s %d \n" , __FUNCTION__ , __LINE__);
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (pUserData)->priv;
	priv->bTooltipShown = TRUE ;
	if( priv->enLauncherState == LAUNCHER_INITIAL || priv->enLauncherState ==  LAUNCHER_SHOWN )
	{
		return;
	}
	v_mildenhall_launcher_expand_stage(pUserData );
}


static void v_drawer_button_close_state_clb (GObject *pActor,  gpointer pUserData)
{
	launcher_debug("\n\n%s %d \n" , __FUNCTION__ , __LINE__);
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (pUserData)->priv;
	 priv->bTooltipShown = FALSE ;
	if( priv->enLauncherState == LAUNCHER_INITIAL || priv->enLauncherState ==  LAUNCHER_SHOWN )
	{
		return;
	}
	 clutter_actor_set_size (stage,WIDTH,HEIGHT);
}


void drawer_button_released(GObject *pActor, gchar *pName, gpointer pUserData)
{
   MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (pUserData)->priv;
   launcher_debug("DRAWER_WIDGET: button released = %s\n", pName);

   if(WIDTH != clutter_actor_get_width(stage) )
   {
	   clutter_actor_set_size (stage,WIDTH,HEIGHT);
   }
   if(g_strcmp0("BACK" ,pName ) == FALSE)
    canterbury_hard_keys_call_back_press(priv->app_mgr_hk_proxy , NULL , NULL , NULL);
   else if(g_strcmp0("HOME" ,pName ) == FALSE)
    canterbury_hard_keys_call_home_press(priv->app_mgr_hk_proxy , NULL , NULL , NULL);
   else if(g_strcmp0("NOW PLAYING" ,pName ) == FALSE)
    canterbury_hard_keys_call_now_playing_press(priv->app_mgr_hk_proxy , NULL , NULL , NULL);
}
/* *********************************************************************
 * Function Name :menu_drawer_button_released_cb
 * Parameters   : ClutterActor,gchar,gpointer.
 * Description  : This callback function is called whenever the menu drawer
 *                buttons are pressed.
 * Return Value : gboolean
 *********************************************************************** */
static gboolean menu_drawer_button_released_cb(ClutterActor *button,gchar *name,gpointer data)
{
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (data)->priv;

	if(!g_strcmp0(name,"SEARCH"))
	{
		mildenhall_speller_show(priv->speller,TRUE);
	}
	return TRUE;
}
/* *********************************************************************
 * Function Name : media_info_create_speller_model
 * Parameters   : MediaInfo Object.
 * Description  : Create the model for speller widget.
 * Return Value : ThornburyModel
 *********************************************************************** */
ThornburyModel *
p_create_speller_model (MildenhallLauncher *launcher)
{
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
	GVariantBuilder *pRowValues = NULL;
	pRowValues = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );

	/* it must read the from the preference manager TODO*/

	g_variant_builder_add(pRowValues, "{ss}", "SEARCH", "SEARCH");

	GVariantBuilder *pRowValues1 = NULL;
	pRowValues1 = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
	g_variant_builder_add(pRowValues1, "{ss}", "Image2",  PKGDATADIR"/icon_search.png");

	GVariant *p = g_variant_builder_end(pRowValues);
	GVariant *q = g_variant_builder_end(pRowValues1);

	/* model for default entry */
	priv->speller_model = (ThornburyModel *)thornbury_list_model_new (SPELLER_COLUMN_LAST,
			G_TYPE_BOOLEAN ,0,
			G_TYPE_FLOAT , 0,
			G_TYPE_STRING, NULL,
			G_TYPE_POINTER, NULL,
			G_TYPE_BOOLEAN ,0,
			G_TYPE_FLOAT , 0,
			G_TYPE_STRING, NULL,
			G_TYPE_POINTER, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			-1);

	/* append model info for default entry */
	thornbury_model_append ( priv->speller_model,
			COLUMN_LEFT_ICON_IS_TEXT ,TRUE,
			COLUMN_LEFT_ICON_WIDTH ,100.0,
			COLUMN_LEFT_ICON_ID,"LEFT",
			COLUMN_LEFT_ICON_INFO, (gpointer)p,
			COLUMN_RIGHT_ICON_IS_TEXT ,FALSE,
			COLUMN_RIGHT_ICON_WIDTH ,60.0,
			COLUMN_RIGHT_ICON_ID,"RIGHT",
			COLUMN_RIGHT_ICON_INFO, (gpointer)q,
			COLUMN_TEXT_BOX_DEFAULT_TEXT, "",
			COLUMN_ENTRY_ID,"entry",
			-1);

	return priv->speller_model;
}

#if 0 
//static gboolean mildenhall_speller_go_cb(ClutterActor *actor, GVariant *text ,gpointer user_data);
/* *********************************************************************
 * Function Name :v_create_speller
 * Parameters   : MediaInfo Object.
 * Description  : This function is used to create the speller widget.
 * Return Value : void
 *********************************************************************** */
static gboolean show_speller(gpointer pUserData)
{
  	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (pUserData)->priv;
        mildenhall_speller_show(priv->speller,TRUE);
        return FALSE;
}
void v_create_speller(MildenhallLauncher *launcher)
{
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
        ThornburyModel *model= p_create_speller_model(launcher);

        ThornburyItemFactory *item_factory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_SPELLER,PKGDATADIR"/launcher_speller_prop.json");
        GObject *pObject = NULL;
        g_object_get(item_factory, "object", &pObject, NULL);

        priv->speller  = CLUTTER_ACTOR(pObject);

        g_object_set(pObject, "model",model,  NULL);

        g_signal_connect(priv->speller, "go-pressed",G_CALLBACK(mildenhall_speller_go_cb), launcher);
        //g_signal_connect_after (priv->speller, "speller-shown", G_CALLBACK (speller_shown_cb), launcher);
        //g_signal_connect_after (priv->speller, "speller-hidden", G_CALLBACK (speller_hidden_cb), launcher);
        clutter_actor_set_position(priv->speller,74.0,458.0);
	clutter_actor_add_child(CLUTTER_ACTOR(launcher),priv->speller);

        //g_timeout_add (1000, show_speller, (gpointer) launcher);
}

/* *********************************************************************
 * Function Name :mildenhall_speller_go_cb
 * Parameters   : actor,text,MoneyConvert Object.
 * Description  : The callback function for the go button of speller.
 * Return Value : gboolean
 *********************************************************************** */
static gboolean mildenhall_speller_go_cb(ClutterActor *actor, GVariant *text ,gpointer user_data)
{
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (user_data)->priv;

  GVariantIter iter;
  gchar *pText = NULL;
  gchar *key = NULL;
  g_variant_iter_init (&iter, text);
  while (g_variant_iter_next (&iter, "{ss}", &key, &pText))
  {
          if(!(g_strcmp0(key,"entry")))
                  priv->pSpellerData = g_strdup(pText);
          if(!(g_strcmp0(key,"LEFT")))
                  priv->pToggleButton = g_strdup(pText);
  }
  e_core_global_search_user_interface_service_call_user_interface_search(priv->proxy,priv->pSpellerData,"all",1,NULL,NULL,NULL);
	mildenhall_speller_hide(actor,TRUE);
  canterbury_mutter_plugin_call_start_idle_animation(priv->mutter_proxy,NULL,NULL,NULL);
	return TRUE;
}
#endif
void populate_rollers( MildenhallLauncher *launcher , gchar **main_menu_label, gchar **main_menu_icon, gchar **quick_menu_label, gchar **quick_menu_icon )
{
	//ClutterActor   *content;

	ClutterActor   *texture;
	GError *err    = NULL;
 	//ClutterColor white = {255, 255, 255, 255};

  	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;

	/*creates the main roller */

	priv->main_menu_model = create_main_menu_model(launcher , main_menu_label , main_menu_icon , quick_menu_label , quick_menu_icon );

	priv->master_roller =  create_roller (priv->main_menu_model);
	clutter_actor_add_child (CLUTTER_ACTOR (launcher), priv->master_roller);

	/* creates Quick menu */
	priv->quick_menu_model = create_quick_menu_model(launcher , main_menu_label , main_menu_icon , quick_menu_label , quick_menu_icon );

	priv->slave_roller = create_slave_roller (priv->quick_menu_model);
	clutter_actor_add_child (CLUTTER_ACTOR (launcher), priv->slave_roller);
	//clutter_actor_set_position(priv->slave_roller, 405,0);

	g_signal_connect (G_OBJECT (priv->master_roller),
	                  "roller-locking-finished",
	                  G_CALLBACK (master_locking_finished_cb),
	                  launcher);

	g_signal_connect (G_OBJECT (priv->master_roller),
	                  "roller-scroll-started",
	                  G_CALLBACK (roller_scroll_started_cb),
	                  launcher);

	g_signal_connect (G_OBJECT (priv->master_roller),
	                  "roller-scroll-finished",
	                  G_CALLBACK (roller_scroll_finished_cb),
	                  launcher);

	g_signal_connect (G_OBJECT (priv->slave_roller),
	                 "roller-locking-finished",
	                 G_CALLBACK (quick_menu_locking_finished_cb),
	                 launcher);

	g_signal_connect (G_OBJECT (priv->slave_roller),
	                 "roller-scroll-started",
	                 G_CALLBACK (roller_scroll_started_cb),
	                 launcher);

	g_signal_connect (G_OBJECT (priv->slave_roller),
	                 "roller-scroll-finished",
	                 G_CALLBACK (roller_scroll_finished_cb),
	                 launcher);

	g_signal_connect (G_OBJECT (priv->slave_roller),
                    "roller-item-activated",//"item-activated",
                    G_CALLBACK (roller_item_activated_cb),
                    launcher);

	/* Appling the overlay */
	texture = clutter_texture_new_from_file (MERGE_MASK3_FILE_NAME, &err);
  if (err)
  {
          g_warning ("unable to load texture: %s", err->message);
          g_clear_error (&err);
          return ;
  }
  clutter_actor_set_position (texture, 0, 0);
 // clutter_actor_set_opacity (texture,0.4);
	clutter_actor_add_child (CLUTTER_ACTOR (launcher), texture);

  texture = clutter_texture_new_from_file (MERGE_MASK4_FILE_NAME, &err);
  if (err)
  {
          g_warning ("unable to load texture: %s", err->message);
          g_clear_error (&err);
          return ;
  }
  clutter_actor_add_child (CLUTTER_ACTOR (launcher), texture);
  clutter_actor_set_position (texture, 800, 2);

  priv->drawer = mildenhall_navi_drawer_new();

  ThornburyModel *navi_drawer_model = create_navi_drawer_model();
  g_object_set(priv->drawer, "model", navi_drawer_model, NULL);

  clutter_actor_add_child (CLUTTER_ACTOR (launcher), priv->drawer);
  clutter_actor_set_position (priv->drawer , 6 ,394);
  g_signal_connect(priv->drawer, "drawer-open", G_CALLBACK(v_drawer_button_open_state_clb), launcher);
  g_signal_connect(priv->drawer, "drawer-tooltip-hidden",  G_CALLBACK(v_drawer_button_close_state_clb), launcher);
  g_signal_connect(priv->drawer, "drawer-close",  G_CALLBACK(v_drawer_button_close_state_clb), launcher);

  g_signal_connect(priv->drawer, "drawer-button-released", G_CALLBACK(drawer_button_released), launcher);


  //v_create_speller(launcher);

  //init_hardkey_client_handler(launcher);
  /* create menu drawer */

  g_timeout_add(1500, set_rollers_to_focus , launcher );

}


static void on_notify_launcher_state(CanterburyMutterPlugin  *object,
                                 guint           launcher_state,
                                 gpointer         user_data)
{
   launcher_debug("on_notify_launcher_state = %d\n", launcher_state);
   MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (user_data)->priv;
   switch(launcher_state)
   {
     case 1:
     {
       clutter_actor_set_position (priv->drawer , 6 ,394);
       clutter_actor_show(priv->drawer);
       clutter_actor_show(priv->menu_drawer_button);
       priv->enLauncherState = LAUNCHER_SHOWN ;
       if(WIDTH != clutter_actor_get_width(stage) )
       {
    	   clutter_actor_set_size (stage,WIDTH,HEIGHT);
       }
     }
     break;

     case 2:
     {
        clutter_actor_hide(priv->drawer);
        clutter_actor_hide(priv->menu_drawer_button);
        priv->enLauncherState = LAUNCHER_ANIMATION_IN_PROGRESS ;
     }
     break;

     case 3:
     {
       clutter_actor_set_position (priv->drawer , 796 ,394);
       clutter_actor_show(priv->drawer);
       priv->enLauncherState = LAUNCHER_MINIMIZED ;
       if( TRUE == priv->bTooltipShown)
       {
    	   /* expand the stage if tooltip is shown before sliding and if still visible after minimizing launcher */
    	   v_mildenhall_launcher_expand_stage(user_data);
       }
     }
     break;
     default:
     break;
   }
}

static void
mutter_proxy_clb( GObject *source_object,
                  GAsyncResult *res,
                  gpointer user_data)
{
	 GError *error;
	 if(! MILDENHALL_LAUNCHER(user_data))
	  return;

	 MildenhallLauncher *launcher = MILDENHALL_LAUNCHER(user_data);
	 MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
	  /* finishes the proxy creation and gets the proxy ptr */
	 priv->mutter_proxy = canterbury_mutter_plugin_proxy_new_finish(res , &error);

	 g_signal_connect (priv->mutter_proxy,
	                   "launcher-state",
	                   G_CALLBACK (on_notify_launcher_state),
	                   user_data);
}
void mutter_name_appeared(GDBusConnection *connection,
                          const gchar     *name,
                          const gchar     *name_owner,
                          gpointer         user_data)
{
	canterbury_mutter_plugin_proxy_new (
                                connection,
                                G_DBUS_PROXY_FLAGS_NONE,
                                "org.apertis.Canterbury.Mutter.Plugin",
                                "/org/apertis/Canterbury/Mutter/Plugin",
                                NULL,
                                mutter_proxy_clb,
                                user_data);
}

void mutter_name_vanished( GDBusConnection *connection,
                            const gchar     *name,
                            gpointer         user_data)
{

}

gboolean is_item_in_model( ThornburyModel* model , const gchar* item )
{
  guint no_of_rows;

  g_return_val_if_fail (model != NULL, FALSE);
  g_return_val_if_fail (item != NULL, FALSE);

  no_of_rows = thornbury_model_get_n_rows (model);

  if( 0 == no_of_rows )
   return FALSE;

  guint cnt = 0;

  for( cnt = 0 ; cnt < no_of_rows ; cnt++ )
  {
    ThornburyModelIter *iter = thornbury_model_get_iter_at_row (model, cnt);
    gchar* value = NULL;
    thornbury_model_iter_get (iter, 0, &value , -1);
    //launcher_debug( "item val name %s \n" , (gchar*)value);
    if( g_strcmp0( value , item ) == FALSE )
    {
      g_free(value);
      break;
    }
    g_free(value);
  }

  if( cnt < no_of_rows )
   return TRUE;

  return FALSE;
}

void
insert_quick_menu_entry_into_model (MildenhallLauncher *launcher, 
                                    ThornburyModel *model, 
                                    const gchar *main_menu, 
                                    const gchar *quick_menu, 
                                    const gchar *quick_menu_icon, 
                                    const gchar *pStatus, 
                                    gdouble progress)
{
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
	gchar *pIconPath = NULL ;
	guint no_of_rows = thornbury_model_get_n_rows(model);
	gchar *quick_menu_first = g_hash_table_lookup(priv->MainToQuickmap,main_menu);
	guint cnt = 0;
	for( cnt = 0 ; cnt < no_of_rows ; cnt++ )
	{
		ThornburyModelIter *iter = thornbury_model_get_iter_at_row (model, cnt);
		gchar* value = NULL;
		thornbury_model_iter_get (iter, 0, &value , -1);
		launcher_debug("quick_menu_first = %s value = %s\n",quick_menu_first,(gchar*) value);
		//launcher_debug( "row name %s \n" , (gchar*)value);
		if( g_strcmp0( value , quick_menu_first ) == FALSE )
		{
			g_free(value);
			break;
		}
		g_free(value);
	}

	for( ; cnt < no_of_rows ; cnt++ )
	{
		ThornburyModelIter *iter = thornbury_model_get_iter_at_row (model, cnt);
		gchar* value = NULL;
		thornbury_model_iter_get (iter, 0, &value , -1);
		//launcher_debug( "row name %s \n" , (gchar*)value);
		gchar *main_menu_next = g_hash_table_lookup(priv->QuickToMain,value);
		g_free(value);
		if( g_strcmp0( main_menu , main_menu_next ) != FALSE )
		{
			break;
		}
		launcher_debug("quick_menu_first = %s value = %s\n",main_menu,(gchar*) main_menu_next);
	}

	launcher_debug( "icon %s \n" , (gchar*)quick_menu_icon);

	LAUNCHER_CHECK_IMAGE_PATH(quick_menu_icon , pIconPath);
	thornbury_model_insert (model, 
				cnt, 
				ILI_COLUMN_NAME, quick_menu, 
				ILI_COLUMN_ICON, pIconPath, 
				ILI_COLUMN_LABEL, quick_menu, 
				ILI_COLUMN_ICON2, pIconPath, 
				ILI_COLUMN_STATUS_TEXT, (pStatus == NULL ? " " : pStatus), 
				ILI_COLUMN_PROGRESS, progress, 
				ILI_COLUMN_LAUNCH_ACTION, APP_LAUNCH_ENABLED, 
				-1);
	LAUNCHER_FREE_MEM_IF_NOT_NULL(pIconPath);
	g_hash_table_insert(priv->QuickToMain,g_strdup(quick_menu),g_strdup(main_menu));

}

void append_new_menu_entry_into_model( MildenhallLauncher* launcher , const gchar* main_menu ,const gchar* main_menu_icon ,
                                       const gchar* quick_menu ,const gchar* quick_menu_icon ,const gchar* pStatus , gdouble progress )
{
	MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
	gchar *pIconPath = NULL ;
	LAUNCHER_CHECK_IMAGE_PATH( main_menu_icon , pIconPath);
	thornbury_model_append (priv->main_menu_model, ILI_COLUMN_NAME, main_menu, ILI_COLUMN_ICON, pIconPath, ILI_COLUMN_LABEL, main_menu,  -1);
	LAUNCHER_FREE_MEM_IF_NOT_NULL(pIconPath);

	g_hash_table_insert(priv->MainToQuickmap,g_strdup(main_menu),g_strdup(quick_menu));

	LAUNCHER_CHECK_IMAGE_PATH( quick_menu_icon , pIconPath);
	thornbury_model_append (priv->quick_menu_model, ILI_COLUMN_NAME, quick_menu, ILI_COLUMN_ICON, pIconPath, ILI_COLUMN_LABEL, quick_menu,
			ILI_COLUMN_ICON2, pIconPath,ILI_COLUMN_STATUS_TEXT , ( pStatus == NULL ? " " : pStatus) , ILI_COLUMN_PROGRESS ,progress, -1);
	LAUNCHER_FREE_MEM_IF_NOT_NULL(pIconPath);
#if 0
	gchar *key = g_strconcat(main_menu,"--",NULL);
	gchar *result_key;
	g_hash_table_lookup_extended(priv->QuickToMain,key,&result_key,NULL);		
	if(result_key != NULL)	
	{
		g_hash_table_remove(priv->QuickToMain,result_key);

		if(result_key != NULL)
		{
			g_free(result_key);
			result_key = NULL;
		}

	}
	if(key != NULL)
	{
		g_free(key);
		key = NULL;
	}
#endif
	g_hash_table_insert(priv->QuickToMain,g_strdup(quick_menu),g_strdup(main_menu));
}

void remove_menu_entry_from_model( MildenhallLauncher* launcher , const gchar *quick_menu )
{
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;

  guint no_of_rows = thornbury_model_get_n_rows(priv->quick_menu_model);
  guint cnt = 0;
  launcher_debug( "remove %s \n" , (gchar*)quick_menu);
  for( cnt = 0 ; cnt < no_of_rows ; cnt++ )
  {
    ThornburyModelIter *iter = thornbury_model_get_iter_at_row (priv->quick_menu_model, cnt);
    gchar* value = NULL;
    thornbury_model_iter_get (iter, 0, &value , -1);
    launcher_debug( "row name %s \n" , (gchar*)value);
    if( g_strcmp0( value , quick_menu ) == FALSE )
    {
      g_free(value); //ramya:is it necessary?

      gchar *main_menu = g_hash_table_lookup(priv->QuickToMain,quick_menu);
      launcher_debug( "main menu name %s \n" , (gchar*)main_menu);
      gchar *quick_menu_first = g_hash_table_lookup(priv->MainToQuickmap,main_menu);
      if( g_strcmp0( quick_menu_first , quick_menu ) == FALSE )
      {
        launcher_debug( "the main to quick connect lost, find a new one \n");
        gchar *new_quick_menu_first = get_new_main_to_quick_map(launcher , main_menu, cnt);

        if(new_quick_menu_first == NULL)
        {
          launcher_debug( "new quick menu doesnt exist.. remove the category from the model \n");
          g_hash_table_remove (priv->MainToQuickmap , main_menu );
         // remove_main_menu_model_item(launcher , main_menu);
        }
        else
        {
          launcher_debug( "new quick menu exists %s replace it with the current one \n" , new_quick_menu_first);
          g_hash_table_replace( priv->MainToQuickmap , g_strdup(main_menu) , g_strdup(new_quick_menu_first) );
        }
      }

      g_hash_table_remove (priv->QuickToMain ,quick_menu );
      thornbury_model_remove (priv->quick_menu_model , cnt);
      break;
    }
    g_free(value);
  }
}

gchar* get_new_main_to_quick_map(MildenhallLauncher* launcher , const gchar *main_menu , guint row_cnt)
{
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
  guint no_of_rows = thornbury_model_get_n_rows(priv->quick_menu_model);

  if(++row_cnt < no_of_rows)
  {
    ThornburyModelIter *iter = thornbury_model_get_iter_at_row (priv->quick_menu_model, row_cnt );
    gchar* next_quick_menu = NULL;
    thornbury_model_iter_get (iter, 0, &next_quick_menu , -1);
    gchar *next_main_menu = g_hash_table_lookup( priv->QuickToMain,next_quick_menu );
    if( g_strcmp0( next_main_menu , main_menu ) == FALSE )
    {
      launcher_debug( "new quick menu %s \n" , next_quick_menu);
      return next_quick_menu;
    }
    else
    {
      return NULL;
    }
  }
  else
  {
    return NULL;
  }
}

void remove_main_menu_model_item(MildenhallLauncher* launcher , const gchar *main_menu)
{
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;

  guint no_of_rows = thornbury_model_get_n_rows(priv->main_menu_model);
  guint cnt = 0;

  for( cnt = 0 ; cnt < no_of_rows ; cnt++ )
  {
    ThornburyModelIter *iter = thornbury_model_get_iter_at_row (priv->main_menu_model, cnt);
    gchar* value = NULL;
    thornbury_model_iter_get (iter, 0, &value , -1);
    //launcher_debug( "row name %s \n" , (gchar*)value);
    if( g_strcmp0( value , main_menu ) == FALSE )
    {
      g_free(value);

      //launcher_debug( "main menu name %s \n" , (gchar*)main_menu);

      g_hash_table_remove (priv->MainToQuickmap ,main_menu );
      thornbury_model_remove (priv->main_menu_model , cnt);
      break;
    }
    g_free(value);
  }
}
