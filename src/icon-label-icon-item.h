/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * icon_label_icon item for MildenhallRoller
 *
 *
 */

#ifndef __ICON_LABEL_ICON_ITEM_H__
#define __ICON_LABEL_ICON_ITEM_H__

#include <clutter/clutter.h>
#include <cogl/cogl.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop
#include <thornbury/thornbury.h>

G_BEGIN_DECLS

#define TYPE_ICON_LABEL_ICON_ITEM (icon_label_icon_item_get_type ())
#define ICON_LABEL_ICON_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_ICON_LABEL_ICON_ITEM, IconLabelIconItem))
#define ICON_LABEL_ICON_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_ICON_LABEL_ICON_ITEM, IconLabelIconItemClass))
#define IS_ICON_LABEL_ICON_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_ICON_LABEL_ICON_ITEM))
#define IS_ICON_LABEL_ICON_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_ICON_LABEL_ICON_ITEM))
#define ICON_LABEL_ICON_ITEM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_ICON_LABEL_ICON_ITEM, IconLabelIconItemClass))

typedef struct _IconLabelIconItem IconLabelIconItem;
typedef struct _IconLabelIconItemClass IconLabelIconItemClass;

enum
{
  ILI_ARROW_WHITE,
  ILI_ARROW_GREY,
  ILI_ARROW_HIDDEN,

  ILI_ARROW_LAST
};

struct _IconLabelIconItem
{
  ClutterGroup group;
  gchar *text;
  gboolean row_called;
  ClutterActor *icon;
  ClutterActor *icon2;
  ClutterActor *label;
  ClutterActor *arrow_up;
  ClutterActor *arrow_down;
  ClutterActor *arrow_right;
  ClutterActor *extra_separator;
  ClutterActor *vertical_line1;
  ClutterActor *vertical_line2;
  ClutterActor *vertical_line3;
  ClutterActor *vertical_line4;
  ClutterActor *horizontal;
  ThornburyModel *model;
  gboolean expanded;

  ClutterActor *pProgress;
  ClutterActor *pStatusLabel;

  ClutterActor *pStatusLabelRight;
  ClutterActor *pProgressRight;

  ClutterEffect *glow_effect_1;
  ClutterEffect *glow_effect_2;
  ClutterEffect *glow_effect_3;
  ClutterEffect *glow_effect_4;
  guint uinAdditionalOption;
};

struct _IconLabelIconItemClass
{
  ClutterGroupClass parent_class;

  // An effect can be attached to only one actor at the same time
};

enum
{
  ILI_COLUMN_NAME,
  ILI_COLUMN_ICON,
  ILI_COLUMN_LABEL,
  ILI_COLUMN_ICON2,
  ILI_COLUMN_STATUS_TEXT,
  ILI_COLUMN_PROGRESS,
  ILI_COLUMN_LAUNCH_ACTION,
  ILI_COLUMN_LAST
};

GType icon_label_icon_item_get_type (void) G_GNUC_CONST;
ClutterActor *icon_label_icon_item_new (void);

G_END_DECLS

#endif /* __ICON_LABEL_ICON_ITEM_H__ */
