m4_define([mildenhall_launcher_version_major], [0])
m4_define([mildenhall_launcher_version_minor], [2020])
m4_define([mildenhall_launcher_version_micro], [1])
m4_define([mildenhall_launcher_version_nano], [0])

m4_define([mildenhall_launcher_version_base_version],
          [mildenhall_launcher_version_major.mildenhall_launcher_version_minor.mildenhall_launcher_version_micro])

m4_define([mildenhall_launcher_version],
          [m4_if(mildenhall_launcher_version_nano, 0,
                [mildenhall_launcher_version_base_version],
                [mildenhall_launcher_version_base_version].[mildenhall_launcher_version_nano])])

AC_INIT([mildenhall_launcher], mildenhall_launcher_version)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AM_MAINTAINER_MODE([enable])

AC_SUBST(MILDENHALL_LAUNCHER_VERSION,mildenhall_launcher_version)
AM_INIT_AUTOMAKE([tar-ustar])

AM_GNU_GETTEXT_VERSION([0.19])
AM_GNU_GETTEXT([external])

GETTEXT_PACKAGE=AC_PACKAGE_TARNAME
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, ["$GETTEXT_PACKAGE"], [Define to the Gettext package name])
AC_SUBST(GETTEXT_PACKAGE)


AC_PROG_CC
AC_PROG_CXX
AC_ISC_POSIX
AC_STDC_HEADERS
AC_SEARCH_LIBS([strerror],[cposix])
AC_CHECK_FUNCS([memset])
AC_CHECK_HEADERS([fcntl.h stdlib.h string.h])

LT_INIT
GLIB_MODULES='glib-2.0 gmodule-2.0 gio-2.0 gthread-2.0 gio-unix-2.0'
AC_SUBST(GLIB_MODULES)
PKG_CHECK_MODULES(GLIB,[$GLIB_MODULES])

DBUS_MODULES='dbus-glib-1 >= 0.98 dbus-1'
AC_SUBST(DBUS_MODULES)
PKG_CHECK_MODULES(DBUS,[$DBUS_MODULES])

pkg_modules="canterbury"
PKG_CHECK_MODULES(CANTERBURY, [$pkg_modules])

pkg_modules="canterbury-platform-0 >= 0.1703.12"
PKG_CHECK_MODULES(CANTERBURY_PLATFORM, [$pkg_modules])

pkg_modules="libthornbury >= 0.1703.0"
PKG_CHECK_MODULES(LIBUIUTILITY, [$pkg_modules])

pkg_modules="liblightwood"
PKG_CHECK_MODULES(LIBWIDGETS, [$pkg_modules])

pkg_modules="mildenhall >= 0.1703.0"
PKG_CHECK_MODULES(MILDENHALL, [$pkg_modules])

pkg_modules="barkway"
PKG_CHECK_MODULES(BARKWAY, [$pkg_modules])

pkg_modules="sqlite3"
PKG_CHECK_MODULES(SQLITE3, [$pkg_modules])

if test "x$GCC" = "xyes"; then
    CFLAGS="$CFLAGS -g -Wall -Wno-unused-but-set-variable -Wl,--no-as-needed"
fi


# Support different levels of compiler error reporting.
# This configure flag is designed to mimic one from gnome-common,
# Defaults to "error" except for releases where it defaults to "yes"
AC_ARG_ENABLE(compile-warnings,
        AS_HELP_STRING([--enable-compile-warnings=@<:@no/minimum/yes/maximum/error@:>@],
                         [Enable different levels of compiler warnings]),,
                 [AS_IF([test "$APERTIS_RELEASE" = "yes"],
                 [enable_compile_warnings="yes"]
                 )])

# Always pass -Werror=unknown-warning-option to get Clang to fail on bad flags.
# $1: Flag to test and append.
# $2: Flags variable to append to.
AC_DEFUN([PM_APPEND_COMPILE_FLAGS],[
 AX_APPEND_COMPILE_FLAGS([$1],[$2],[-Werror=unknown-warning-option])
])

PM_APPEND_COMPILE_FLAGS([-fno-strict-aliasing],[ERROR_CFLAGS])
PM_APPEND_COMPILE_FLAGS([-g],[ERROR_CFLAGS],[-Werror=unknown-warning-option])
PM_APPEND_COMPILE_FLAGS([-Wl,--no-as-needed],[ERROR_LDFLAGS])

AS_IF([test "$enable_compile_warnings" != "no"],[
 PM_APPEND_COMPILE_FLAGS([-Wall],[ERROR_CFLAGS])
])
AS_IF([test "$enable_compile_warnings" != "no" -a \
 "$enable_compile_warnings" != "minimum"],[
 PM_APPEND_COMPILE_FLAGS([ dnl
		 -Wextra dnl
                 -Wundef dnl
                 -Wnested-externs dnl
                 -Wwrite-strings dnl
                 -Wpointer-arith dnl
                 -Wmissing-declarations dnl
                 -Wmissing-prototypes dnl
                 -Wstrict-prototypes dnl
                 -Wredundant-decls dnl
                 -Wno-unused-parameter dnl
                 -Wno-missing-field-initializers dnl
                 -Wdeclaration-after-statement dnl
                 -Wformat=2 dnl
                 -Wold-style-definition dnl
                 -Wcast-align dnl
                 -Wformat-nonliteral dnl
                 -Wformat-security dnl
                 ],[ERROR_CFLAGS])
])

AS_IF([test "$enable_compile_warnings" = "yes" -o \
 "$enable_compile_warnings" = "maximum" -o \
 "$enable_compile_warnings" = "error"],[
 PM_APPEND_COMPILE_FLAGS([ dnl
                 -Wsign-compare dnl
                 -Wstrict-aliasing dnl
                 -Wshadow dnl
                 -Winline dnl
                 -Wpacked dnl
                 -Wmissing-format-attribute dnl
                 -Wmissing-noreturn dnl
                 -Winit-self dnl
                 -Wredundant-decls dnl
                 -Wmissing-include-dirs dnl
                 -Wunused-but-set-variable dnl
                 -Warray-bounds dnl
         ],[ERROR_CFLAGS])
])
AS_IF([test "$enable_compile_warnings" = "maximum" -o \
 "$enable_compile_warnings" = "error"],[
 PM_APPEND_COMPILE_FLAGS([ dnl
 -Wswitch-enum dnl
 -Wswitch-default dnl
 -Waggregate-return dnl
 ],[ERROR_CFLAGS])
])

AS_IF([test "$enable_compile_warnings" = "error"],[
 AX_APPEND_FLAG([-Werror],[ERROR_CFLAGS])
 PM_APPEND_COMPILE_FLAGS([ dnl
 -Wno-suggest-attribute=format dnl
 ],[ERROR_CFLAGS])
])

# Installed tests
AC_ARG_ENABLE(modular_tests,
         AS_HELP_STRING([--disable-modular-tests],
         [Disable build of test programs (default: no)]),,
         [enable_modular_tests=yes])

AC_ARG_ENABLE(installed_tests,
         AS_HELP_STRING([--enable-installed-tests],
         [Install test programs (default: no)]),,
         [enable_installed_tests=no])

AM_CONDITIONAL(BUILD_MODULAR_TESTS,
         [test "$enable_modular_tests" = "yes" ||
         test "$enable_installed_tests" = "yes"])
AM_CONDITIONAL(BUILDOPT_INSTALL_TESTS, test "$enable_installed_tests" = "yes")

# code coverage
AX_CODE_COVERAGE

AC_SUBST([ERROR_CFLAGS])
AC_SUBST([ERROR_LDFLAGS])

AC_SUBST(GCC_FLAGS)

AC_CONFIG_FILES([
    Makefile
    src/Makefile
    resources/Makefile
    resources/data/Makefile
    resources/data/active/Makefile
    resources/data/inactive/Makefile
    scripts/Makefile
	po/Makefile.in
])

AC_PATH_PROG(DBUS_BINDING_TOOL, dbus-binding-tool)

AC_OUTPUT

dnl Summary
echo "                      mildenhall-launcher "
echo "                      --------------"
echo ""
